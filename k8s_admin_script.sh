# bash <(curl -s https://gitlab.com/ariemwn/tools/-/raw/main/k8s_admin_script.sh)
#!/bin/bash

# Fungsi untuk menambahkan warna
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
reset=$(tput sgr0)

# Skrip ini akan mencetak berbagai perintah dan komentar mereka.

# Cetak Perintah untuk Melihat Semua Node
echo "${green}# Melihat semua node dalam kluster Kubernetes${reset}"
echo "${yellow}kubectl get nodes${reset}"
echo ""

# Cetak Perintah untuk Melihat Semua Pod
echo "${green}# Melihat semua pod di semua namespace dalam kluster Kubernetes${reset}"
echo "${yellow}kubectl get pods --all-namespaces${reset}"
echo ""

# Cetak Perintah untuk Drain Node
echo "${green}# Melakukan drain pada sebuah node (gantilah [NODE_NAME] dengan nama node yang sesuai)${reset}"
echo "${yellow}kubectl drain [NODE_NAME] --delete-local-data --force --ignore-daemonsets${reset}"
echo ""

# Cetak Perintah untuk Reset Kluster
echo "${green}# Melakukan reset kluster menggunakan kubeadm${reset}"
echo "${yellow}sudo kubeadm reset${reset}"
echo ""

# Cetak Perintah untuk Inisialisasi Master Node
echo "${green}# Inisialisasi kluster dari awal dengan master node${reset}"
echo "${yellow}sudo kubeadm init --control-plane-endpoint=k8smaster.example.net${reset}"
echo ""

echo "${green}# Aksi: Instalasi Flannel setelah menginisialisasi kluster.${reset}"
echo "${yellow}kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml${reset}"
echo ""

# Cetak Perintah untuk Deploy Calico
echo "${green}# NEW Menginstal Calico sebagai CNI${reset}"
echo "${yellow}kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml${reset}"
echo ""

# Cetak Perintah untuk Deploy Calico
echo "${green}# OLD Menginstal Calico sebagai CNI${reset}"
echo "${yellow}kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml${reset}"
echo ""

echo "${green}# Aksi: Menghapus Calico${reset}"
echo "${yellow}kubectl delete -f https://docs.projectcalico.org/manifests/calico.yaml${reset}"
echo ""

# Cetak Perintah untuk Periksa Status Master Node
echo "${green}# Melihat status master node menggunakan systemctl${reset}"
echo "${yellow}sudo systemctl status kubelet${reset}"
echo ""

# Cetak Perintah untuk Periksa Status Firewall
echo "${green}# Melihat status firewall pada node master atau worker${reset}"
echo "${yellow}sudo ufw status${reset}"
echo ""

# Langkah-langkah untuk Troubleshooting: Ini akan menampilkan 20 baris log terakhir dari kubelet.${reset}"
echo "${green}# Menampilkan log kubelet terbaru${reset}"
echo "${yellow}journalctl -u kubelet -n 20 --no-pager${reset}"
echo ""

echo "${green}# Melihat file konfigurasi kubelet${reset}"
echo "${yellow}cat /etc/systemd/system/kubelet.service.d/10-kubeadm.conf${reset}"
echo ""



