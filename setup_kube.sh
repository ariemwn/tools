#!/bin/bash
# bash <(curl -s https://gitlab.com/ariemwn/tools/-/raw/main/setup_kube.sh)
set -e # Exit script jika terjadi error

# Cek IP yang ada di mesin
ip_list=$(ip -4 a | grep inet | awk '{ print $2 }' | cut -d'/' -f1 | grep -v '127.0.0.1')

# Variabel hostname dan IP
ip_address="192.168.1.5"
cidr="192.168.0.0"

# Verifikasi IP
ip_valid=false

# Fungsi untuk menampilkan IP yang aktif
show_active_ips() {
  echo "IP yang aktif di mesin ini:"
  for ip in $ip_list; do
    echo $ip
  done
}

while [ "$ip_valid" = false ]; do
  echo "Alamat IP $ip_address tidak tepat di mesin ini. Mohon perbaiki."
  show_active_ips
  
  # Menerima input baru dari pengguna
  read -p "Masukkan IP yang valid: " new_ip
  ip_address=$new_ip

  # Verifikasi IP baru
  for ip in $ip_list; do
    if [[ "$ip" == "$ip_address" ]]; then
      ip_valid=true
      break
    fi
  done
done

# Loop untuk validasi input role
while true; do
  read -p "Apakah ini master atau worker node? (master/worker): " role
  if [[ "$role" == "master" || "$role" == "worker" ]]; then
    break
  else
    echo "Input salah. Hanya 'master' atau 'worker' yang diperbolehkan."
  fi
done

hostname="k8$role.example.net"

# Cek jika role adalah master dan apakah sudah ada cluster control panel
if [[ "$role" == "master" ]]; then
  if kubectl get componentstatuses &> /dev/null; then
    echo "Sudah ada cluster control panel pada server ini. Saran: Rejoin cluster yang ada."
    exit 1
  fi
fi

# Fungsi untuk menampilkan pesan dengan warna
print_message() {
  local color
  case "$1" in
    "success") color="\e[32m" ;; # Hijau
    "warning") color="\e[33m" ;; # Kuning
    "critical") color="\e[31m" ;; # Merah
    *) color="\e[0m"  ;; # Default (tidak ada warna)
  esac
  echo -e "${color}$2\e[0m"
}

# Fungsi untuk memeriksa apakah langkah telah dijalankan sebelumnya
check_step() {
  if [[ -f "$1" ]]; then
    echo "Langkah $2 sudah dijalankan sebelumnya. Melewatinya..."
    return 0
  else
    return 1
  fi
}

execute_step() {
    local step_file=$1
    local step_num=$2
    local commands=$3
    
    # Memeriksa apakah langkah ini telah dijalankan sebelumnya
    if check_step "$step_file" "$step_num"; then
        return
    fi
    
    echo "Menjalankan Langkah $step_num..."
    
    # Menjalankan perintah dan menyimpan outputnya
    output=$(eval "$commands" 2>&1)
    
    # Mengecek status eksekusi perintah
    if [ $? -eq 0 ]; then
        echo "Langkah $step_num berhasil."
        echo "$output" > "$step_file"
    else
        echo "Langkah $step_num gagal. Lihat output berikut untuk debugging:"
        echo "$output"
        exit 1
    fi
}


# Langkah 1: Set hostname pada node-master
execute_step "step1.txt" 1 "sudo hostnamectl set-hostname '$hostname'; exit"

# Mengecek apakah IP dan hostname sudah ada di /etc/hosts
if ! grep -q "$ip_address   $hostname" /etc/hosts; then
  execute_step "step3.txt" 3 "sudo sh -c 'echo \"$ip_address   $hostname $hostname\" >> /etc/hosts'"
else
  echo "Entri untuk $hostname dengan IP $ip_address sudah ada di /etc/hosts. Melewatkan langkah ini."
fi

# Langkah 4: Nonaktifkan swap dan tambahkan parameter kernel
# Mengecek apakah /etc/fstab sudah dimodifikasi
if ! grep -q "^#.*swap.img" /etc/fstab; then
    execute_step "step4.txt" 4 "sudo swapoff -a; sudo sed -i '/swap.img/s/^/#/' /etc/fstab; sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter
"
else
    echo "Perubahan fstab untuk menonaktifkan swap sudah ada. Melewatkan langkah ini."
fi

# Langkah 5: Set parameter kernel untuk Kubernetes
execute_step "step5.txt" 5 "sudo tee /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sudo sysctl --system"

# Langkah 6: Install container runtime (Containerd)
execute_step "step6.txt" 6 "sudo apt update -y; sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates; sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg; echo | sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu \$(lsb_release -cs) stable\"; sudo apt update -y; sudo apt install -y containerd.io; containerd config default | sudo tee /etc/containerd/config.toml > /dev/null; sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml; sudo systemctl restart containerd; sudo systemctl enable containerd"

# Langkah 7: Add Apt Repository for Kubernetes
execute_step "step7.txt" 7 "echo | curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/kubernetes-xenial.gpg; echo | sudo apt-add-repository \"deb http://apt.kubernetes.io/ kubernetes-xenial main\"; sudo apt update"

# Langkah 8: Install Kubectl, Kubeadm, and Kubelet
execute_step "step8.txt" 8 "sudo apt install -y kubelet kubeadm kubectl; sudo apt-mark hold kubelet kubeadm kubectl"

# Langkah 9: Initialize Kubernetes Cluster with Kubeadm
if [ "$role" == "master" ]; then
  # execute_step "step9.txt" 9 "sudo kubeadm init --control-plane-endpoint=$hostname > kubeadm_init_output.txt"
  execute_step "step9.txt" 9 "sudo kubeadm init --pod-network-cidr=$cidr/16 --control-plane-endpoint=$hostname > kubeadm_init_output.txt"
fi

if [ "$role" == "master" ]; then
  execute_step "step10.txt" 10 "mkdir -p $HOME/.kube; sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config; sudo chown $(id -u):$(id -g) $HOME/.kube/config"

# Menambahkan kode tambahan
echo "export KUBECONFIG=$HOME/.kube/config" >> ~/.bashrc
source ~/.bashrc
echo $KUBECONFIG

fi

# Langkah 11: Konfigurasi kubectl
if [ "$role" == "worker" ]; then
  execute_step "step11.txt" 11 "echo 'Jalankan perintah berikut di setiap worker node: sudo kubeadm join $hostname:6443 --token <token> --discovery-token-ca-cert-hash <hash>'"
fi

# Langkah 12: Menggunakan Kubectl untuk Melihat Informasi Cluster
if [ "$role" == "master" ]; then
  execute_step "step12.txt" 12 "echo 'Menggunakan Kubectl untuk Melihat Informasi Cluster: kubectl cluster-info; kubectl get nodes'"
fi

# Langkah 13: Deploy pod network
if [ "$role" == "master" ]; then
  execute_step "step13.txt" 13 "echo 'Deploy pod network: kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml'"
fi

# Langkah 14: Verifikasi Status Pods dalam Namespace kube-system
execute_step "step14.txt" 14 "echo 'Verifikasi Status Pods dalam Namespace kube-system: kubectl get pods -n kube-system'"

# Langkah 13: Deploy pod network
if [ "$role" == "master" ]; then
echo "Langkah 15: Uji Instalasi Kubernetes Cluster"
# execute_step "step15.txt" 15 "kubectl create deployment nginx-app --image=nginx --replicas=2; kubectl get deployment nginx-app; kubectl expose deployment nginx-app --type=NodePort --port=80; kubectl get svc nginx-app; kubectl describe svc nginx-app; echo 'Gunakan perintah berikut untuk mengakses aplikasi nginx:'; echo '\$ curl http://<worker-node-ip-address>:<nodeport>'; echo 'Contoh:'; echo '\$ curl http://192.168.1.174:31246'"
echo "kubectl create deployment nginx-app --image=nginx --replicas=2"
echo "kubectl get deployment nginx-app"
echo "kubectl expose deployment nginx-app --type=NodePort --port=80"
echo "kubectl get svc nginx-app"
echo "kubectl describe svc nginx-app"
echo "Gunakan perintah berikut untuk mengakses aplikasi nginx:"
echo "curl http://<worker-node-ip-address>:<nodeport>"
echo "Contoh:"
echo "curl http://192.168.1.174:31246"
fi

# Akhir dari skrip
if [ "$role" == "master" ]; then
  print_message "success" "Instalasi dan konfigurasi lengkap Kubernetes telah berhasil dilakukan."
fi

# Akhir dari skrip
if [ "$role" == "worker" ]; then
  print_message "success" "Perintah join ada pada file master kubeadm_init_output.txt jangan lupa tambah HOSTS master sebelum join"
fi
