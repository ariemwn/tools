#!/bin/bash
# bash <(curl -s https://gitlab.com/ariemwn/tools/-/raw/main/manage_kube.sh)

# Mengecek komponen awal setup
check_initial_setup() {
  # Cek kubectl
  if ! command -v kubectl &> /dev/null; then
    echo "kubectl tidak ditemukan! Silakan install terlebih dahulu."
    return 1
  fi

  # Cek node
  NODES=$(kubectl get nodes --no-headers 2>/dev/null)
  if [[ -z "$NODES" ]]; then
    echo "Tidak ada node yang terhubung! Silakan setup node terlebih dahulu."
    return 1
  fi

  # Cek apakah Calico terinstall
  CALICO=$(kubectl get daemonsets -n kube-system | grep calico-node)
  if [[ -z "$CALICO" ]]; then
    echo "JOIN DULU salah satu NODE! Calico tidak terinstall. Mohon install terlebih dahulu."
    echo "kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.1/manifests/calico.yaml"
    return 1
  fi
  echo "Semua komponen awal terinstall dengan baik."
  return 0

}

# Menampilkan pods
lihat_pods() {
  echo "Melihat status pod pada namespace kube-system..."
  kubectl get pods -n kube-system
}

# Menampilkan nodes
lihat_nodes() {
  echo "Melihat semua node..."
  kubectl get nodes
}

# Mengecek dan memanage node dengan status NotReady
manage_notready_nodes() {
  NOT_READY_NODES=$(kubectl get nodes --no-headers | awk '$2!="Ready"{print $1}')
  if [[ -z "$NOT_READY_NODES" ]]; then
    echo "Tidak ada node dengan status NotReady."
  else
    echo "Ada node dengan status NotReady:"
    echo "$NOT_READY_NODES"
    for NODE in $NOT_READY_NODES; do
      echo "Menampilkan detil dari node $NODE..."
      kubectl describe node "$NODE"
    done
  fi
}

# Mengecek dan memanage pod dengan status Pending
manage_pending_pods() {
  PENDING_PODS=$(kubectl get pods --all-namespaces --no-headers | awk '$4=="Pending"{print $2 " " $1}')
  if [[ -z "$PENDING_PODS" ]]; then
    echo "Tidak ada pod dengan status Pending."
  else
    echo "Ada pod dengan status Pending:"
    echo "$PENDING_PODS"
    echo "$PENDING_PODS" | while read -r POD_NAME NAMESPACE; do
      echo "Menampilkan detil dari pod $POD_NAME di namespace $NAMESPACE..."
      kubectl describe pod "$POD_NAME" -n "$NAMESPACE"
    done
  fi
}

# Fungsi untuk mengelola status pod yang tidak biasa
manage_unusual_pod_status() {
  UNUSUAL_STATUS=$(kubectl get pods --all-namespaces --no-headers | awk '$4!="Running" && $4!="Pending"{print $2 " " $1 " " $4}')
  if [[ -z "$UNUSUAL_STATUS" ]]; then
    echo "Tidak ada pod dengan status tidak biasa."
  else
    echo "Ada pod dengan status tidak biasa:"
    echo "$UNUSUAL_STATUS"
    echo "$UNUSUAL_STATUS" | while read -r POD_NAME NAMESPACE STATUS; do
      echo "Menampilkan detil dari pod $POD_NAME di namespace $NAMESPACE dengan status $STATUS..."
      kubectl describe pod "$POD_NAME" -n "$NAMESPACE"
    done
  fi
}

# Menampilkan storage
lihat_storage() {
  echo "Melihat storage..."
  kubectl get pv,pvc
}

# Menampilkan network
lihat_network() {
  echo "Melihat network..."
  kubectl get svc,ingress
}

# Menampilkan plugin
lihat_plugin() {
  echo "Melihat plugin..."
  kubectl get ds -n kube-system
}

# Menampilkan services
lihat_services() {
  echo "Melihat services..."
  kubectl get svc
}

# Melihat semua namespaces
lihat_namespaces() {
  echo "Melihat semua namespaces..."
  kubectl get namespaces
}

# Melihat status cluster
lihat_cluster_info() {
  echo "Melihat informasi cluster..."
  kubectl cluster-info
}

# Menjalankan cek kesehatan cluster
cek_kesehatan() {
  echo "Menjalankan cek kesehatan cluster..."
  kubectl get componentstatuses
}

# Melihat resource quota
lihat_resource_quota() {
  echo "Melihat resource quotas..."
  kubectl get resourcequota --all-namespaces
}

# Debug node yang NotReady
debug_not_ready_nodes() {
  echo "Debugging node dengan status NotReady..."
  kubectl get nodes --selector='!node-role.kubernetes.io/master' -o custom-columns='NAME:.metadata.name,STATUS:.status.conditions[-1].type'
}

# Cek kenapa pod Pending
cek_pod_pending() {
  echo "Mengecek pod yang dalam status Pending..."
  kubectl get pods --all-namespaces --field-selector=status.phase=Pending
}

# Menu utama
echo "Manajer Cluster Kubernetes"
PS3='Pilih opsi: '
opsi=("Check Setup" "Lihat Pods" "Lihat Nodes" "manage nodes" "manage pods" "manage unusual pod" "Lihat Storage" "Lihat Network" "Lihat Plugin" "Lihat Services" "Lihat Namespaces" "Lihat Cluster Info" "Cek Kesehatan Cluster" "Lihat Resource Quota" "Debug Node NotReady" "Cek Pod Pending" "Keluar")
select pilihan in "${opsi[@]}"
do
  case $pilihan in
    "Check Setup")
      check_initial_setup
      ;;    
    "Lihat Pods")
      lihat_pods
      ;;
    "Lihat Nodes")
      lihat_nodes
      ;;
    "manage nodes")
      manage_notready_nodes
      ;;
    "manage pods")
      manage_pending_pods
      ;;
    "manage unusual pod")
      manage_unusual_pod_status
      ;;
    "Lihat Storage")
      lihat_storage
      ;;
    "Lihat Network")
      lihat_network
      ;;
    "Lihat Plugin")
      lihat_plugin
      ;;
    "Lihat Services")
      lihat_services
      ;;
    "Lihat Namespaces")
      lihat_namespaces
      ;;
    "Lihat Cluster Info")
      lihat_cluster_info
      ;;
    "Cek Kesehatan Cluster")
      cek_kesehatan
      ;;
    "Lihat Resource Quota")
      lihat_resource_quota
      ;;
    "Debug Node NotReady")
      debug_not_ready_nodes
      ;;
    "Cek Pod Pending")
      cek_pod_pending
      ;;
    "Keluar")
      break
      ;;
    *) echo "Opsi tidak valid $REPLY";;
  esac
done
