# cat change_ip.sh
# Menjalankan Skrip Langsung
# bash <(curl -s https://gitlab.com/ariemwn/tools/-/raw/main/ubahip.sh)
# bash <(wget -O - https://gitlab.com/ariemwn/tools/-/raw/main/ubahip.sh)

#!/bin/bash

# Menampilkan dan menyimpan IP saat ini
current_ip=$(ip -4 addr show enp0s2 | grep inet | awk '{print $2}' | cut -d '/' -f 1)
if [ -z "$current_ip" ]; then
    echo "Tidak ada IP yang dikonfigurasi saat ini."
    current_ip="none"
else
    echo "IP saat ini: $current_ip"
fi

# Menerima IP baru dan gateway dalam satu baris, dipisahkan oleh koma
read -p "Masukkan IP baru dan gateway, dipisahkan oleh koma (Contoh: 192.168.1.2,192.168.1.1): " input
IFS=',' read -ra ADDR <<< "$input"
new_ip="${ADDR[0]}"
new_gateway="${ADDR[1]}"

# Validasi alamat
if [[ -z "$new_ip" || -z "$new_gateway" ]]; then
    echo "IP atau gateway tidak boleh kosong."
    exit 1
fi

# Input IP baru dari pengguna
# read -p "Masukkan IP baru: " new_ip

# Nama interface, ganti sesuai dengan sistem Anda
interface="enp0s2"

# Tambahkan IP baru tanpa menghapus yang lama
sudo ip addr add $new_ip/24 dev $interface

# Ubah IP secara permanen dalam Netplan
netplan_config="/etc/netplan/01-netcfg.yaml"

# Backup konfigurasi Netplan sebelumnya
sudo cp $netplan_config "$netplan_config.bak"

# Buat file konfigurasi Netplan baru
echo "network:" > $netplan_config
echo "  version: 2" >> $netplan_config
echo "  renderer: networkd" >> $netplan_config
echo "  ethernets:" >> $netplan_config
echo "    $interface:" >> $netplan_config
echo "      dhcp4: no" >> $netplan_config
echo "      addresses: [$new_ip/24]" >> $netplan_config
echo "      gateway4: $new_gateway" >> $netplan_config


# Terapkan perubahan
sudo netplan apply

# Hapus IP lama
read -p "Apakah Anda sudah memastikan bahwa IP baru bekerja? (y/n): " confirmation
if [ "$confirmation" == "y" ]; then
  sudo ip addr del $current_ip/24 dev $interface
  sudo sed -i "/$current_ip/d" $netplan_config
  sudo netplan apply
else
  echo "Konfigurasi belum diubah. Silakan cek IP baru sebelum melanjutkan."
fi
